import { Component,OnInit } from '@angular/core';
import { TramiteService } from '../services/tramites.service';
import { DepartamentoService } from '../services/departamento.service';
import { MunicipioService } from '../services/municipio.service';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { convertActionBinding } from '@angular/compiler/src/compiler_util/expression_converter';
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  
  public url:string='';
  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';
  public codDepartamento : number = 0;
  public tramites : Array<any>=[]
  public departamentos : Array<any>=[]
  public municipios : Array<any>=[]
  constructor(
    private http: HttpClient,
    private tramiteService : TramiteService,
    private departamentoService : DepartamentoService,
    private municipioService : MunicipioService
  ) {
    this.tramiteService.getTramite().subscribe((resp:any) =>{
      console.log(resp)
      this.tramites=resp
    })
    this.departamentoService.getDepartamento().subscribe((dep:any) =>{
      console.log(dep)
      this.departamentos=dep
    })
  }

  capturar() {
    for (var v in this.departamentos) 
    {  
      if(this.departamentos[v].depNombre==this.opcionSeleccionado)
      {
        this.codDepartamento = Number(this.departamentos[v].depCodigo)
      } 
    }
    this.municipioService.getMunicipio(this.codDepartamento).subscribe((mun:any) =>{
      console.log(mun)
      this.municipios=mun
    })  
}
  ngOnInit(): void {
  }
}
